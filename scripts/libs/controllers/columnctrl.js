window.App.controller("ColumnCtrl", ["$scope", "$http", "$rootScope",
    function($scope, $http, $rootScope) {

        $scope.test = "aw";

        $scope.init = function() {
            $http({
                method: "GET",
                url: window._ACCESS_URL
            }).then(function(response) {
                $rootScope.allIssues = response.data;
            });
        };

        $scope.init();
    }
]);