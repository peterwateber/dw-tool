<?php

class MY_Core extends CI_Controller {
    
    var $data = array();
    
    function __construct() {
        parent::__construct();
    }
    
    
    function _response($arg, $is_json = FALSE) {
        if (!$is_json) {
            echo $arg;
            exit;
        }
        echo json_encode($arg);
    }
    
}
