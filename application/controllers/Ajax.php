<?php

class Ajax extends MY_Core {
    
    private $request;
    
    function __construct() {
        parent::__construct();
        $this->request = $this->input->get('action');
    }
    
    function index() {
        
        $arr = array(
            'To do' => array(
                view(TEMPLATES_DIR . 'card', TRUE),
                view(TEMPLATES_DIR . 'card', TRUE),
                view(TEMPLATES_DIR . 'card', TRUE)
            )
        );
        $this->_response($arr, TRUE);
    }
    
}
