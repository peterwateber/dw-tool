<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
            $styles = array(
                CSS_DIR . 'bootstrap.min.css',
                CSS_DIR . 'fa.css',
                CSS_DIR . 'style.css',
                CSS_DIR . 'navbar.css'
            );
            
            foreach($styles as $css) {
                echo link_tag($css);
            }
        ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php view(MISC_DIR . 'header'); ?>