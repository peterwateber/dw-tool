<?php

class TemplatesLib {
    
    static $CI;
    
    function __construct() {
        self::$CI =& get_instance();
    }
    
    static function card() {
        return view(TEMPLATES_DIR . 'card.php');
    }
}
