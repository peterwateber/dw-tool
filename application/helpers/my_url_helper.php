<?php

if (!function_exists('view')) {
    function view($page, $is_html = FALSE) {
        $_this = &get_instance();
        return $_this->load->view($page, $_this->data, $is_html);
    }
}